/* 

1-  Fazer com que cada célula do labirinto seja uma DIV (loop for e dom ?)
2-  Fazer de cada linha do labirindo um div usando display flex
3-  faça o DIV do jogador ser (appended) a uma célula DIV pelo mesmo motivo.
4-  Registrar ou receber via solitação a posição atual do jogador (indice de linha e 
    coluna). Voce poderia atualizar o array de mapa constantemente para refletir
    o movimento do jogador. (mover o "S" pelo mapa). Posso manter os indices em atributos
    de dados em HTML e acessalos atraves da div de jogador "parentElement"(no caso do 3b). 
4.2-Ou posso fazer um pouco de matematica na posição atual do div do jogador na tela
    relativo a posição inicial do elemento de inicio na tela e o tamanho das
    celulas (no caso 3A)
5-  O movimento pode ser realziado de algumas maneiras diferentes no caso do 3A
    mudar a posição absoluta do div do jgoador. ou 3b faça append do Div
    do jogador ao proximo 
    div de celula.
6-  posso usar queryslector e seletor de css para obter o proximo elemento
    de celula pelos indices)
    */

/* 
   Requisitos
   1-    O jogador deve começar no quadrado inicial.
   2-    Configure um ou mais handlers de evento para mover seu DIV de jogador da mesma
   forma que fez nas avaliações anteriores sobre eventos de teclado.
   3-    Não permita que o jogador atravesse uma parede ou saia dos limites do labirinto.
   4-    Quando o jogador se mover para o quadrado final, mostre ao usuário que ele venceu
   (não use console.log ou alertas para isso).
   */

/* AQUI TEMOS O MAPA. */

const map = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];
console.log(map);

// div global do mapa.





//--- Function jogo cria o tabuleiro labirinto e coloca o boneco na sua posição incicial.

function jogo () {
    let jogador = document.getElementById("jogador")
    
    
    for (let l = 0; l < map.length; l++) {

        let criarLinha = document.createElement("div");
        criarLinha.className = "lines"

        document.getElementById("global").appendChild(criarLinha);
        
        for (let c = 0; c < map[l].length; c++) {
            
            let criarCelula = document.createElement("div");
            
            criarCelula.id = "linha" + l + "coluna" + c ;
            criarLinha.appendChild(criarCelula);
            
            if (map[l][c] == " " || map[l][c] == "S" || map[l][c] == "F") {
                criarCelula.className = "caminho"
            }
            if (map[l][c] == "W" ) {
                criarCelula.className = "parede"
            } 
            
            
            
            // [j] = " " string 
            // div do espaço em branco / 
        }
    } 
    document.getElementById("linha9coluna0").appendChild(jogador)
}
jogo()


// let criarColuna = document.createElement("div")
// criarColuna.className = "coluna"
// document.getElementById("global").appendChild(criarColuna)

"use strict";



/* 
se for cima / linha - 
se for baixo / linha +


se for direita  / coluna +
se for esquerda / coluna -
 */

let linha  = 9   // linha8coluna20 = posição inicial do boneco.
let coluna = 0



//------------ EVENT KEY. MOVIMENTAÇÃO ----------------------------------------

document.addEventListener("keydown", (event) => {

    let divAtual  // divAtual = div atual em que se encontra a movimentação do jogador.
    let player = document.getElementById("jogador") // pegando id da DIV Jogador.
    const nomeDaTecla = event.key;  
  
    
    console.log("keydown event\n\n" + "key: " + nomeDaTecla); // mostrando no console qual tecla está sendo pressionada.
   

    //logica abaixo: se apertar TECLA vai aumentar ou diminuir coluna ou linha.
    //se a proxima div conter classe 'caminho' ele fará o append, caso contrario não.

    if (nomeDaTecla === "ArrowDown") {
        
        event.preventDefault()
        linha +=1
        divAtual =  document.getElementById('linha' + linha + 'coluna' + coluna )

                if (divAtual.classList.contains('caminho') ) {
                                
                    document.getElementById('linha' + linha + 'coluna' + coluna ).appendChild(player)
            
            
                    
                }
                else {
                    linha -= 1
                }

    }
    if (nomeDaTecla === "ArrowUp") {

        event.preventDefault()

        linha -=1
        divAtual =  document.getElementById('linha' + linha + 'coluna' + coluna )

                if (divAtual.classList.contains('caminho') ) {
                    
                    
                    document.getElementById('linha' + linha + 'coluna' + coluna ).appendChild(player)
            
            
                    
                }
                else {
                    linha += 1
                }

    }
    if (nomeDaTecla === "ArrowRight") {

        event.preventDefault()      
        coluna +=1
        divAtual =  document.getElementById('linha' + linha + 'coluna' + coluna )

        //linha9coluna1

                if (divAtual.classList.contains('caminho') ) {
                    
                    document.getElementById('linha' + linha + 'coluna' + coluna ).appendChild(player)
            
            
                    
                }
                else {
                    coluna -= 1
                }

    }
    if (nomeDaTecla === "ArrowLeft") {
        
        event.preventDefault()

        coluna -=1;
        
        divAtual =  document.getElementById('linha' + linha + 'coluna' + coluna )
        
        if (divAtual.classList.contains('caminho') ) {
            
            document.getElementById('linha' + linha + 'coluna' + coluna ).appendChild(player)
        
            
            
            
        }
        else {
            coluna += 1
        }
        
    }
    //---------linha de chegada 
    
    let zombie = document.getElementById("jogador")
    
    function eatBrains () {
        //linha9coluna2
        let linhaDeChegada = document.getElementById('linha8coluna20')


        if (zombie.parentElement == linhaDeChegada) {
            document.getElementById("gameOver").style.display = "initial"
            
        }
    }
    eatBrains()

});

// --------------------- fim da movimentação.


// ------ start game ---------
function start() {
    document.getElementById("header").style.display = "none"

}




// ---------- JOGAR NOVAMENTE
function jogarNovamente() {
    document.location.reload();
    document.getElementById("gameOver").style.display = "none"
    
}
